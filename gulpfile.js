var gulp  = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var browserify = require('browserify');
var transform = require('vinyl-transform');
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var vueify = require('vueify')
//var babelify = require('babelify');

gulp.task('sass', function(){
  return gulp.src('resources/assets/sass/app.scss')
  .pipe(sass())
  .pipe(gulp.dest('resources/assets/css'))
});

gulp.task('css', ['sass'], function(){
  return gulp.src(['resources/assets/css/libs/*.css', 'resources/assets/css/app.css'])
  .pipe(concat('app.css'))
  //  .pipe(cleanCSS())
  .pipe(gulp.dest('public/css'));
});

gulp.task('browserify', function () {
  return browserify('resources/assets/js/app.js')
    .transform(vueify)
  //  .transform(babelify)
    .bundle()
    .pipe(source('browserified.js'))
    .pipe(gulp.dest('resources/assets/browserified'));
});

gulp.task('js', ['browserify'], function() {
  return gulp.src(['resources/assets/js/libs/*.js', 'resources/assets/browserified/browserified.js'])
  //.pipe(sourcemaps.init())
  .pipe(concat('app.js'))
  //.pipe(uglify())
  //.pipe(sourcemaps.write())
  .pipe(gulp.dest('public/js'));
});

gulp.task('watch-sass', function() {
  return gulp
  .watch('resources/assets/sass/*.scss', ['sass', 'css'])
});

gulp.task('watch-js', function() {
  return gulp
  .watch('resources/assets/js/*.js', ['browserify', 'js'])
});

gulp.task('watch-vue', function() {
  return gulp
  .watch('resources/assets/js/components/*.vue', ['browserify', 'js'])
});

gulp.task('default', ['watch-sass', 'watch-js', 'watch-vue', 'sass', 'css', 'js', 'browserify']);
