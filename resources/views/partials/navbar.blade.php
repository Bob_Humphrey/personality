<?php

$quizLink = url('/quiz');

// Set the current page as active.
$activeQuiz = '';
$path = ($_SERVER['REQUEST_URI']);

if (substr($path, 0, strlen($quizLink)) == $quizLink) {
  $activeQuiz = 'active';
}
?>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://bobhumphrey.org"><img src="/images/logo-80.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/">
            <span class="nav-item visible-lg-inline">PERSONALITY QUIZ</span>
          </a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="{{$activeQuiz}}"><a href="{{$quizLink}}">START NEW QUIZ</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
