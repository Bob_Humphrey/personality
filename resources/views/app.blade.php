<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Personality Quiz</title>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/css/app.css">
  <link rel="icon" type="image/x-icon" href="/favicon.ico" />
</head>
<body>
  @include('partials.navbar')
  <section id="content">
    <div class="container @yield('pageClass')">
      @include('partials.alerts')
      <div id="app"></div>
    </div>
  </section>
  @include('partials.footer')
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
  <script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
