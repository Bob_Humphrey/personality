@extends('app')

@section('pageClass') quiz-page @stop

@section('content')

<div class="container content personality-quiz">
  <quiz
  :question-number="questionNumber"
  :question="question"
  :measures="measures"
  >
</quiz>
</div>

@stop
