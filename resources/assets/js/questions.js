module.exports = [
  {
    trait1: 'bored by time alone',
    trait2: 'needs time alone',
    dichotomyKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'accepts things as they are',
    trait2: 'unsatisfied with the way things are',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'sceptical',
    trait2: 'wants to believe',
    dichotomyKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'makes lists',
    trait2: 'relies on memory',
    dichotomyKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'energetic',
    trait2: 'mellow',
    dichotomyKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'prefers to take multiple choice test',
    trait2: 'prefers essay answers',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'thinks "robotic" is an insult',
    trait2: 'strives to have a mechanical mind',
    dichotomyKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'keeps a clean room',
    trait2: 'just puts stuff where ever',
    dichotomyKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'works best in groups',
    trait2: 'works best alone',
    dichotomyKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'focused on the present',
    trait2: 'focused on the future',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'easily hurt',
    trait2: 'thick-skinned',
    dichotomyKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'chaotic',
    trait2: 'organized',
    dichotomyKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'gets worn out by parties',
    trait2: 'gets fired up by parties',
    dichotomyKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'fits in',
    trait2: 'stands out',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'wants people\'s respect',
    trait2: 'wants their love',
    dichotomyKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'plans far ahead',
    trait2: 'plans at the last minute',
    dichotomyKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'talks more',
    trait2: 'listens more',
    dichotomyKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'when describing an event, will tell what happened',
    trait2: 'when describing an event, will tell what it meant',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'wants to be good at fixing things',
    trait2: 'wants to be good at fixing people',
    dichotomyKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'keeps options open',
    trait2: 'commits',
    dichotomyKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'stays at home',
    trait2: 'goes out on the town',
    dichotomyKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'wants the big picture',
    trait2: 'wants the details',
    dichotomyKey: 'SN',
    operation: -1,
    answer: null
  },
  {
    trait1: 'follows the heart',
    trait2: 'follows the head',
    dichotomyKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'gets work done right away',
    trait2: 'procrastinates',
    dichotomyKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'finds it difficult to yell loudly',
    trait2: 'yelling to others when they are far away comes naturally',
    dichotomyKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'theoretical',
    trait2: 'empirical',
    dichotomyKey: 'SN',
    operation: -1,
    answer: null
  },
  {
    trait1: 'bases morality on justice',
    trait2: 'bases morality on compassion',
    dichotomyKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'improvises',
    trait2: 'prepares',
    dichotomyKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'likes to perform in front of other people',
    trait2: 'avoids public speaking',
    dichotomyKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'likes to know "who?", "what?", "when?"',
    trait2: 'likes to know "why?"',
    dichotomyKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'uncomfortable with emotions',
    trait2: 'values emotions',
    dichotomyKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'works hard',
    trait2: 'plays hard',
    dichotomyKey: 'JP',
    operation: 1,
    answer: null
  },
]
