var Vue = require('vue');
var Vuex = require('vuex');
//Vue.config.debug = true;
var App = require('./components/App.vue');
var Quiz = require('./components/Quiz.vue');
var Home = require('./components/Home.vue');
var Results = require('./components/Results.vue');

var VueRouter = require('vue-router');
Vue.use(VueRouter);

var router = new VueRouter();
router.map({
    '/home': {
        component: Home
    },
    '/results': {
        component: Results
    },
    '/quiz': {
        component: Quiz
    }
});
router.beforeEach(function () {
  window.scrollTo(0, 0)
});
router.redirect({
  '*': '/home'
});
router.start(App, '#app');
