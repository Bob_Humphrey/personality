function saveAnswer (store, question, answer) {
  store.dispatch('SAVE_ANSWER', question, answer);
}

function saveDichotomyChartData (store, key, rightValue, leftValue) {
  store.dispatch('SAVE_DICHOTOMY_CHART_DATA', key, rightValue, leftValue);
}

function saveResults (store, results) {
  store.dispatch('SAVE_RESULTS', results);
}

exports.saveAnswer = saveAnswer;
exports.saveDichotomyChartData = saveDichotomyChartData;
exports.saveResults = saveResults;
