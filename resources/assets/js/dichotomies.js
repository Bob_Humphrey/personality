module.exports = {
  JP: {
    name: 'JP',
    leftValueLabel: 'Judging',
    rightValueLabel: 'Perceiving',
    chartID: 'jpChart',
    dichotomyCounter: 0,
    results: 18,
    averages: [18, 21, 24, 21, 24, 21, 24, 21, 24],
    score: 0,
    chartData:  [
      {
        value: 12,
        color: "#0D47A1",
      },
      {
        value: 12,
        color:"#039BE5",
      },
    ],
  },
  FT: {
    name: 'FT',
    leftValueLabel: 'Feeling',
    rightValueLabel: 'Thinking',
    chartID: 'ftChart',
    dichotomyCounter: 0,
    results: 30,
    averages: [30, 27, 30, 33, 30, 27, 30, 27, 24],
    score: 0,
    chartData:  [
      {
        value: 12,
        color: "#0D47A1",
      },
      {
        value: 12,
        color:"#039BE5",
      },
    ],
  },
  IE: {
    name: 'IE',
    leftValueLabel: 'Introversion',
    rightValueLabel: 'Extroversion',
    chartID: 'ieChart',
    dichotomyCounter: 0,
    results: 30,
    averages: [30, 27, 24, 21, 24, 21, 24, 27, 24],
    score: 0,
    chartData:  [
      {
        value: 12,
        color: "#0D47A1",
      },
      {
        value: 12,
        color:"#039BE5",
      },
    ],
  },
  SN: {
    name: 'SN',
    leftValueLabel: 'Sensing',
    rightValueLabel: 'Intuition',
    chartID: 'snChart',
    dichotomyCounter: 0,
    results: 12,
    averages: [12, 15, 18, 21, 24, 27, 24, 21, 24],
    score: 0,
    chartData:  [
      {
        value: 12,
        color: "#0D47A1",
      },
      {
        value: 12,
        color:"#039BE5",
      },
    ],
  },
}
