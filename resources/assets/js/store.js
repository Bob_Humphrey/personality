var Vue = require('vue');
var Vuex = require('vuex');
Vue.use(Vuex);
var d = require ('./dichotomies.js');
var q = require ('./questions.js');

const state = {
  count: 99,
  cat: "Felix",
  dog: "Snoopy",
  dichotomies: d,
  questions: q,
  answers: [],
  results: '',
}

const mutations = {
  INCREMENT (state) {
    state.count++
  },
  SAVE_ANSWER (state, question, answer) {
    //state.answers.push(answer);
    state.questions[question].answer = answer;
  },
  SAVE_DICHOTOMY_CHART_DATA (state, key, rightValue, leftValue) {
    state.dichotomies[key].chartData[0].value = rightValue;
    state.dichotomies[key].chartData[1].value = leftValue;
  },
  SAVE_RESULTS ( state, results ) {
    state.results = results;
  }

}

module.exports = new Vuex.Store({
  state,
  mutations
})
